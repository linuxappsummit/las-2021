Attendance

* Aleix
* Caroline
* Molly
* Neofytos
* Sri

## Overview of how LAS fundraising works
* we maintain a list of potential sponsors
* we write a draft email and send it to them
* Profit

## Short post-mortem of LAS2020
* Could have been a more organized and started earlier
* Not enough outreach for new sponsors
* more of a focus on relationship management
* people liked office hours!
  * should we run booths? uncertain. leaning towards no.
  * we ran out of slots last year :( (or yay?)
* We started late, but we're earlier this year?
* We do well with consultancies & distros

## Sri's ideas
* Start fundraising for 2022 now
  * We seem medium on starting that now, but mentioning that we're planning on LAS 2022, please keep us in mind when building budgets.
  * Add to brochure 2022 sponsorship and that we can have a sponsorship for 2022 at an, e.g. 5% discount, if you pledge to sponsor both years
* Consider sponsorships around our running a track at a conference
  * potentially dicey and might upset conferences
  * This would be reframing LAS sponsorship as "Sponsoring all LAS activities in 2021"
  * Raise prices
  * Two proposed models: "LAS is also an event sponsored by X, Y, and Z" OR checking with conference for permission
  * Use messaging around sponsorship also bringing LAS to other places
* Franchising the LAS name
  * Sri thinks this would be attractive for places with less infrastructure
  * Comes with, e.g., use of BBB and indico
  * Has anyone asked to do this? No?
  * To discuss for the future

## review sponsorship brochure and track requested updates
* to do on own time

## Review ask list
* to do on own time

## Tasks
* list who you know on sponsor list  https://docs.google.com/spreadsheets/d/1M4OD_6UsRfo42PUFWCVjmh7cvDG97H0j8RkBL3ERh-Q/edit?usp=sharing (everyone)
* add to sponsor list (everyone)
* move text to something/google doc to review and edit (mdb)
* review and edit brochure text (everyone)
* Add 2022 sponsorship to brochure (Caroline)
* draft an ask email (Sri)

